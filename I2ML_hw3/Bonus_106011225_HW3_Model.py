# -*- coding: utf-8 -*-

import pandas as pd

A = pd.read_csv('pred_initW.csv')
B = pd.read_csv('pred_Adagrad.csv')
C = pd.read_csv('hw2_pred.csv')

for i in range(len(A)):
    A.iloc[i,1] += B.iloc[i, 1] + C[ C['PATIENT ID'] == A.iloc[i,0]].iloc[0, 1]

    if A.iloc[i, 1] >= 2:
        A.iloc[i, 1] = 1
    else:
        A.iloc[i, 1] = 0

A.to_csv('Bonus_106011225.csv', index=False)

A
