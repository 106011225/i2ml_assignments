# -*- coding: utf-8 -*-

import pandas as pd

A = pd.read_csv('pred_initW.csv')
B = pd.read_csv('pred_NoinitW.csv')
C = pd.read_csv('pred_Adagrad.csv')

A['hospital_outcome'] = A['hospital_outcome'] + B['hospital_outcome'] + C['hospital_outcome']

for i in range(len(A)):
    if A.iloc[i, 1] >= 2:
        A.iloc[i, 1] = 1
    else:
        A.iloc[i, 1] = 0

A.to_csv('106011225.csv', index=False)
