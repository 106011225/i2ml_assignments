# -*- coding: utf-8 -*-

#mount & cp data

from google.colab import drive
drive.mount('/content/gdrive')

!cp -r /content/gdrive/MyDrive/Colab\ Notebooks/I2ML/HW3/Data/ .

#import

import torch
from PIL import Image
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
from torch.utils.data import Dataset, DataLoader
import torchvision
from torchvision import transforms
import torch.nn as nn
import random
import math


imagePath_train = '/content/Data/IML_CXR_TRAIN/'
imagePath_test = '/content/Data/IML_CXR_TEST/'
labelPath_train = '/content/Data/cxr_label_train.csv'

'''
imagePath_train = '/content/gdrive/MyDrive/Colab\ Notebooks/I2ML/HW3/Data/IML_CXR_TRAIN/'
imagePath_test = '/content/gdrive/MyDrive/Colab\ Notebooks/I2ML/HW3/Data/IML_CXR_TEST/'
labelPath_train = '/content/gdrive/MyDrive/Colab\ Notebooks/I2ML/HW3/Data/cxr_label_train.csv'
'''

device = torch.device('cuda:0')
'''
import os
assert os.environ['COLAB_TPU_ADDR'], 'Make sure to select TPU from Edit > Notebook settings > Hardware accelerator'

!pip install cloud-tpu-client==0.10 https://storage.googleapis.com/tpu-pytorch/wheels/torch_xla-1.7-cp36-cp36m-linux_x86_64.whl

import torch_xla
import torch_xla.core.xla_model as xm

device = xm.xla_device()
'''

# read files, split out validation & augmentation of training set


#testing set
Ans_pd = pd.read_csv('/content/Data/prediction_example.csv')
Ans_np = Ans_pd.to_numpy()

#training & validation set 
data_Y_origin = pd.read_csv(labelPath_train)
data_Y_origin = data_Y_origin.sort_values(by=['PATIENT ID'])
print('total set:\n', data_Y_origin['hospital_outcome'].value_counts(), '\n')

data_Y_origin = data_Y_origin.to_numpy()

train_Y_origin = data_Y_origin[ : math.floor(len(data_Y_origin) * 0.80), : ]
valid_Y_origin = data_Y_origin[math.floor(len(data_Y_origin) * 0.80): , : ]
print('training set:\n',pd.DataFrame(train_Y_origin).iloc[:,1].value_counts(),'\n')
print('validation set:\n',pd.DataFrame(valid_Y_origin).iloc[:,1].value_counts(), '\n\n')

#augmentation
trainSetIndex_LT = []
for patient in train_Y_origin:
    id = patient[0]
    outcome = patient[1] 

    if outcome == 1:
        for counter in range(64):
            LT_item = {}
            LT_item['id'] = id
            LT_item['outcome'] = outcome
            LT_item['transList'] = {}
            LT_item['transList']['deg'] =    (counter >> 0) % 2
            LT_item['transList']['shift'] =  (counter >> 1) % 2
            LT_item['transList']['scale'] =  (counter >> 2) % 2
            LT_item['transList']['flip'] =   (counter >> 3) % 2
            LT_item['transList']['bright'] = (counter >> 4) % 2
            LT_item['transList']['crop'] =   (counter >> 5) % 2

            trainSetIndex_LT.append(LT_item)
    else: #outcome == 0
        LT_item = {}
        LT_item['id'] = id
        LT_item['outcome'] = outcome
        LT_item['transList'] = {}
        LT_item['transList']['deg'] =    0
        LT_item['transList']['shift'] =  0
        LT_item['transList']['scale'] =  0
        LT_item['transList']['flip'] =   0
        LT_item['transList']['bright'] = 0
        LT_item['transList']['crop'] =   0

        trainSetIndex_LT.append(LT_item)

        for counter in range(9):
            LT_item = {}
            LT_item['id'] = id
            LT_item['outcome'] = outcome
            LT_item['transList'] = {}
            LT_item['transList']['deg'] =    random.randint(0,1)
            LT_item['transList']['shift'] =  random.randint(0,1)
            LT_item['transList']['scale'] =  random.randint(0,1)
            LT_item['transList']['flip'] =   random.randint(0,1)
            LT_item['transList']['bright'] = random.randint(0,1)
            LT_item['transList']['crop'] =   random.randint(0,1)

            trainSetIndex_LT.append(LT_item)


print('size of training data after augmentation:',len(trainSetIndex_LT))

#def class: training, validation, testing dataset

Nmean = (0.485 + 0.456 + 0.406) / 3
Nstd = (0.229 + 0.224 + 0.225) / 3

class Dataset_train(Dataset):
    def __init__(self, imgsize=224):
        self.patientList = pd.read_csv(labelPath_train)
        self.patientList = self.patientList.sort_values(by=['PATIENT ID'])
        self.len = len(trainSetIndex_LT)

        self.Tscale = transforms.RandomAffine(degrees=0, scale=(1.1, 1.1))
        self.Tshift = transforms.RandomAffine(degrees=0, translate=(0.1, 0.1))
        self.Tdeg = transforms.RandomAffine(degrees=10)
        self.Tcrop = transforms.RandomCrop(size=300)
        self.Tbright = transforms.ColorJitter(brightness=0.2)
        self.Tflip = transforms.RandomHorizontalFlip(p=1)

        self.Tresize = transforms.Resize(size=(imgsize,imgsize))
        self.Ttotensor = transforms.ToTensor()
        self.Tnormalize = transforms.Normalize(mean=[Nmean], std=[Nstd])

    def __len__(self):
        return self.len

    def __getitem__(self, idx):
        item = trainSetIndex_LT[idx]
        patientID = item['id']

        y = item['outcome']
        if y == 0:
            y = torch.tensor([1., 0.])
        else:
            y = torch.tensor([0., 1.])

        x = Image.open(imagePath_train + str(item['id']) + '.jpg')

        if item['transList']['scale']:
            x = self.Tscale(x)
        if item['transList']['shift']:
            x = self.Tshift(x)
        if item['transList']['deg']:
            x = self.Tdeg(x)
        if item['transList']['crop']:
            x = self.Tcrop(x)
        if item['transList']['bright']:
            x = self.Tbright(x)
        if item['transList']['flip']:
            x = self.Tflip(x)

        x = self.Tresize(x)
        x = self.Ttotensor(x)
        x = self.Tnormalize(x)

        return patientID, x, y

  
class Dataset_valid(Dataset):
    def __init__(self, imgsize=224):
        self.patientList = valid_Y_origin
        self.len = len(valid_Y_origin)

        self.Tresize = transforms.Resize(size=(imgsize,imgsize))
        self.Ttotensor = transforms.ToTensor()
        self.Tnormalize = transforms.Normalize(mean=[Nmean], std=[Nstd])

    def __len__(self):
        return self.len

    def __getitem__(self, idx):

        patientID = self.patientList[idx][0]
        
        y = self.patientList[idx][1]
        if y == 0:
            y = torch.tensor([1., 0.])
        else:
            y = torch.tensor([0., 1.])

        x = Image.open(imagePath_train + str(patientID) + '.jpg')
        x = self.Tresize(x)
        x = self.Ttotensor(x)
        x = self.Tnormalize(x)

        return patientID, x, y


class Dataset_test(Dataset):
    def __init__(self, imgsize=224):
        self.patientList = Ans_np
        self.len = len(Ans_np)

        self.Tresize = transforms.Resize(size=(imgsize,imgsize))
        self.Ttotensor = transforms.ToTensor()
        self.Tnormalize = transforms.Normalize(mean=[Nmean], std=[Nstd])

    def __len__(self):
        return self.len

    def __getitem__(self, idx):

        patientID = int(self.patientList[idx][0])
        
        x = Image.open(imagePath_test + str(patientID) + '.jpg')
        x = self.Tresize(x)
        x = self.Ttotensor(x)
        x = self.Tnormalize(x)

        return x




# construct training, validation, testing dataset
dataset_train = Dataset_train(imgsize=224)
dataset_valid = Dataset_valid(imgsize=224)
dataset_test = Dataset_test(imgsize=224)

#def: F1_Score

class F1_Score(nn.Module):
    def __init__(self, epsilon=1e-7):
        super().__init__()
        self.epsilon = epsilon
    
    def forward(self, y_pred, y_true):

        y_pred = (y_pred[:, 1] > 0.5).astype('int32')
        y_pred = y_pred.reshape(-1)
        y_true = y_true[:, 1].astype('int32')
        y_true = y_true.reshape(-1)
        
        #print(y_true[0:20])
        #print(y_pred[0:20])

        tp = float((y_true * y_pred).sum())
        tn = float(((1 - y_true) * (1 - y_pred)).sum())
        fp = float(((1 - y_true) * y_pred).sum())
        fn = float((y_true * (1 - y_pred)).sum())

        precision = tp / (tp + fp + self.epsilon)
        recall = tp / (tp + fn + self.epsilon)
    
        f1 = 2* (precision*recall) / (precision + recall + self.epsilon)
        return f1

#construct a model to be trained

model = torchvision.models.resnet50(pretrained=True)
pretrainedModel_outputSize = model.fc.in_features

# get init weights of conv1
init_weight = []
for kernelth in model.conv1.state_dict()['weight']:
    init_weight.append(kernelth.mean(dim=0).unsqueeze(0).numpy())
init_weight = torch.tensor(init_weight)


# requires_grad = False
for param in model.parameters():
    param.requires_grad = False


# replae layers
model.conv1 = nn.Conv2d(1, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
model.conv1.state_dict()['weight'] = init_weight
model.fc = nn.Sequential(
              nn.Linear(in_features=pretrainedModel_outputSize, out_features=12),
              nn.ReLU(),
              nn.Dropout(0.4),
              nn.Linear(in_features=12, out_features=2),
              nn.Softmax(dim=1)
           )


total_params = sum(p.numel() for p in model.parameters())
print(f'{total_params:,} total parameters.')
total_trainable_params = sum(
    p.numel() for p in model.parameters() if p.requires_grad)
print(f'{total_trainable_params:,} training parameters.')

# training

model.to(device)

model.train()
print("model.training =", model.training)



f1_score = F1_Score()
criterion = nn.BCELoss()
optimizer = torch.optim.Adam(model.parameters())

dataloader_train = DataLoader(dataset=dataset_train, batch_size=100, shuffle=True)
dataloader_valid = DataLoader(dataset=dataset_valid, batch_size=100, shuffle=False)

n_epochs = 30
loss_list = []

earlyStoppingFlag = False
last_valid_f1 = -1.0

for epoch in range(n_epochs):
    print("now in epoch =", epoch)

    #training
    batchCnt = 0
    for id, x, y in dataloader_train:
        id, x, y = id.to(device), x.to(device), y.to(device)
        optimizer.zero_grad()
        yhat = model(x)
        loss = criterion(yhat, y)
        if (batchCnt % 10) == 0:
            print('f1 score of training set batch: [%.6f]'%f1_score( yhat.cpu().detach().numpy() , y.cpu().detach().numpy() ), " loss: [%.6f]"%float(loss))
        loss.backward()
        optimizer.step()
        loss_list.append(float(loss))
        del id, x, y, yhat, loss

        batchCnt += 1

    #validation dropout
    model.eval()
    print("model.training =", model.training)

    with torch.no_grad():
        y_pred_list = []
        y_true_list = []
        for idv, xv, yv in dataloader_valid:
            xv, yv = xv.to(device), yv.to(device)
            yhatv = model(xv)
            
            y_pred = list((yhatv.cpu().detach().numpy()[:, 1] > 0.5).astype('int32').reshape(-1))
            y_true = list((yv.cpu().detach().numpy()[:, 1] > 0.5).astype('int32').reshape(-1))
            y_pred_list.extend(y_pred)
            y_true_list.extend(y_true)
            
            del idv, xv, yv, yhatv

        y_pred_list = np.array(y_pred_list)
        y_true_list = np.array(y_true_list)

        print(y_pred_list.shape, y_pred_list)

        tpv = float((y_true_list * y_pred_list).sum())
        tnv = float(((1 - y_true_list) * (1 - y_pred_list)).sum())
        fpv = float(((1 - y_true_list) * y_pred_list).sum())
        fnv = float((y_true_list * (1 - y_pred_list)).sum())

        precisionv = tpv / (tpv + fpv + 1e-7)
        recallv = tpv / (tpv + fnv + 1e-7)
    
        f1 = 2* (precisionv*recallv) / (precisionv + recallv + 1e-7)
        print('***f1 score of validation set batch:', f1)
     
    model.train()
    print("model.training =", model.training)
    '''
        if f1 > last_valid_f1:
            last_valid_f1 = f1
        else:
            earlyStoppingFlag = True
            break
    '''



plt.plot(loss_list)
plt.show()

#torch.cuda.empty_cache() 
!nvidia-smi

# 3min / epoch

# testing & save prediction



model.eval()
print("model.training =", model.training)

#Ans_pd = pd.read_csv('/content/Data/prediction_example.csv')
#Ans_np = Ans_pd.to_numpy()

with torch.no_grad():
    for i in range(len(dataset_test)):
        x = dataset_test[i].unsqueeze(0)
        x = x.to(device)
        yhat = model(x)
        Ans_pd.iloc[i,1] = int((yhat.cpu().detach().numpy()[:, 1] > 0.5).astype('int32').reshape(-1)[0])
        del x, yhat

Ans_pd['hospital_outcome'] = Ans_pd['hospital_outcome'].astype('int32')

Ans_pd.to_csv('pred_initW.csv', index=False)

Ans_pd

# def showImage

Ttotensor = transforms.ToTensor()

def showImage(x):
    plt.imshow(x, cmap='gray', vmin=0, vmax=255)
    plt.title('untitled')
    plt.show()
    print(Ttotensor(x))



"""

==========BACKUP==========

"""

'''
class Model(nn.Module):
    def __init__(self, out_1=32, out_2=48):
        super(Model, self).__init__()
        self.cnn1 = nn.Conv2d(in_channels=1, out_channels=out_1, kernel_size=5, padding=0)
        self.maxpool1 = nn.MaxPool2d(kernel_size=2)
        self.cnn2 = nn.Conv2d(in_channels=out_1, out_channels=out_2, kernel_size=5, stride=1, padding=1)
        self.maxpool2 = nn.MaxPool2d(kernel_size=2)
        self.fc1 = nn.Linear(out_2*78*78, 1)
        self.ac1 = nn.Sigmoid()

    def forward(self, x):
        x = self.cnn1(x)
        x = torch.relu(x)
        x = self.maxpool1(x)
        x = self.cnn2(x)
        x = torch.relu(x)
        x = self.maxpool2(x)
        x = x.view(x.size(0), -1)
        x = self.fc1(x)
        x = self.ac1(x)
        x = (x > 0.5).to(torch.float32)
        x.requires_grad = True
        return x
'''

#torch.save(model, '/content/model_ver1.pt')
'''
model2 = torch.load('/content/model_ver1.pt')
model2.eval()
print("model2.training() =", model2.training())

model2.cpu()
'''

'''
f1_socre = F1_Score()
dataloader_valid = DataLoader(dataset=dataset_valid, batch_size=100, shuffle=True)

with torch.no_grad():
    yhat_list = []
    y_list = []
    for i in range(len(dataset_valid)):
        _, x, y = dataset_valid[i]
        x, y = x, y
        x = x.view(1, 1, 224, 224)
        #yhat = model(x)
        yhat = model2(x)
        y_list.append(y.tolist())
        yhat_list.append(yhat.tolist())



f1_socre(torch.tensor(yhat_list), torch.tensor(y_list))
'''
