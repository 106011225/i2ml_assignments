# Machine learning assignments: COVID-19 30-day mortality prediction from CXR


## Goal 

- Build a machine learning model to predict the 30-day mortality of patients
    - That is, whether a patient will die in the hospital within 30 days
- Use chest X-ray (CXR) images for model training
- This is a CNN-based (**ResNet50**) model


## Dataset

- There are 1393 patients in the training dataset and each with a CXR JPG image and its file name is an unique patient ID
    - The size of CXR image has been processed to 320x320
    - Each image has been processed with histogram equalization
- 1393 hospital outcome for all patients will be provided and used as the labels

Part of dataset:  
<img src="./assets/data_screenshot.png" width="500">


## Usage

1. Run [106011225_HW3_Model.py](./106011225_HW3_Model.py) 
    ```
    $ python 106011225_HW3_Model.py
    ```

    The output file is [pred_initW.csv](./outputs/pred_initW.csv)



**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
