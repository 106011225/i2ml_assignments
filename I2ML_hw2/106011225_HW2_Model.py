# -*- coding: utf-8 -*-

#define

import pandas as pd
import numpy as np
import csv
import math

import torch
import torch.nn as nn
import torch.nn.functional as F

train_X_csv_name = 'hm_hospitales_covid_structured_30d_train.csv'
train_Y_csv_name = 'split_train_export_30d.csv'
test_X_csv_name  = 'fixed_test.csv'

# preprocessing

train_X_pd = pd.read_csv(train_X_csv_name)

train_X_pd.drop(['PATIENT ID', 'admission_datetime'], axis=1, inplace=True)

train_X_pd['age'] = train_X_pd['age'].astype('float64')

train_X_pd.loc[ train_X_pd['sex'] == 'MALE', ['sex']] = 0
train_X_pd.loc[ train_X_pd['sex'] == 'FEMALE', ['sex']] = 1
train_X_pd['sex'] = train_X_pd['sex'].astype('float64')

# fill blanks

for i in range(len(train_X_pd.loc[0,'vitals_temp_ed_first':'lab_hemoglobin'])):
    median = train_X_pd.iloc[:,i+3].median()
    train_X_pd.iloc[train_X_pd.iloc[:,i+3].isnull(), i+3] = median



# trans train_X_pd['ed_diagnosis'] to onehot
dia_rowNum = len(train_X_pd)
dia_colNum = train_X_pd['ed_diagnosis'].nunique()
dia_onehot_np = np.zeros([dia_rowNum, dia_colNum])

for i in range(dia_rowNum):
    if (train_X_pd.loc[i, 'ed_diagnosis'] == 'sx_breathing_difficulty'):
        dia_onehot_np[i, 0] = 1
    elif (train_X_pd.loc[i, 'ed_diagnosis'] == 'sx_fever'):
        dia_onehot_np[i, 1] = 1
    elif (train_X_pd.loc[i, 'ed_diagnosis'] == 'sx_flu'):
        dia_onehot_np[i, 2] = 1
    elif (train_X_pd.loc[i, 'ed_diagnosis'] == 'sx_cough'):
        dia_onehot_np[i, 3] = 1
    else: # sx_others
        dia_onehot_np[i, 4] = 1

dia_onehot_pd = pd.DataFrame(dia_onehot_np)
dia_onehot_pd = dia_onehot_pd.rename(columns={0:'sx_breathing_difficulty', 1:'sx_fever', 2:'sx_flu', 3:'sx_cough', 4:'sx_others'})

train_X = pd.concat([train_X_pd, dia_onehot_pd], axis=1)
train_X.drop(['ed_diagnosis'], axis=1, inplace=True)


#normalize
for i in range(len(train_X.iloc[0,:])):
    if (train_X.iloc[:, i].max() > 1):
        mean = train_X.iloc[:, i].mean()
        std = train_X.iloc[:, i].std()
        train_X.iloc[:, i] = (train_X.iloc[:, i] - mean) / std


#convert to numpy
train_X = train_X.to_numpy()


# ground truth
train_Y_pd = pd.read_csv(train_Y_csv_name)
train_Y_pd.drop(['PATIENT ID'], axis=1, inplace=True)
train_Y = train_Y_pd.to_numpy()

# split out validation set
valid_X = train_X[math.floor(len(train_X) * 0.85): , : ]
valid_Y = train_Y[math.floor(len(train_Y) * 0.85): , : ]
train_X = train_X[ : math.floor(len(train_X) * 0.85), : ]
train_Y = train_Y[ : math.floor(len(train_Y) * 0.85), : ]


# convert to torch tensor
train_X = torch.tensor(train_X, requires_grad=True).float()
train_y = torch.tensor(train_Y)
train_Y = torch.zeros(len(train_y), 2) # onehot
for i in range(len(train_Y)):
    train_Y[i, train_y[i]] = 1

valid_X = torch.tensor(valid_X).float()
valid_y = torch.tensor(valid_Y)
valid_Y = torch.zeros(len(valid_y), 2)
for i in range(len(valid_Y)):
    valid_Y[i, valid_y[i]] = 1




print('shape of train_X:', train_X.shape, '\tshape of train_Y:', train_Y.shape)
print('shape of valid_X:', valid_X.shape, '\tshape of valid_Y:', valid_Y.shape)

# def function for calculating precision & recall
def evalGDness(prediction, groundTruth):
    TN = FN = FP = TP = 0
    #print(prediction.shape, groundTruth.shape)
    for i in range(len(prediction)):
        #print(prediction[i, 0], prediction[i, 1], groundTruth[i, 0], groundTruth[i, 1])
        if (prediction[i, 0] >= 0.5 and groundTruth[i, 0] == 1):
            TN += 1
        elif (prediction[i, 0] >= 0.5 and groundTruth[i, 1] == 1):
            FN += 1
        elif (prediction[i, 0] < 0.5 and groundTruth[i, 0] == 1):
            FP += 1
        else:
            TP += 1
    
    precision = TP / (FP + TP)
    recall = TP / (FN + TP)

    return [precision, recall]

# NN

sizeInput = len(train_X[0])
sizeHidden1 = 64
sizeHidden2 = 32
sizeHidden3 = 12
sizeOutput = 2

stepSize = 0.001
momentum = 0.9
numEpoch = 1200

models = []

for i in range(7): # add
  model = nn.Sequential(
                    nn.Linear(sizeInput, sizeHidden1),
                    #nn.Dropout(p=0.3),
                    nn.ReLU(),
                    nn.Linear(sizeHidden1, sizeHidden2),
                    nn.Dropout(p=0.5),
                    nn.ReLU(),
                    nn.Linear(sizeHidden2, sizeHidden3),
                    nn.Dropout(p=0.5),
                    nn.ReLU(),
                    nn.Linear(sizeHidden3, sizeOutput),
                    #nn.Dropout(p=0.5),
                    nn.Softmax(dim=1)
                    )

  lossFunc = nn.BCELoss()
  optim = torch.optim.Adam(model.parameters(), lr=stepSize)
  


  gtp = []
  gtr = []
  gvp = []
  gvr = []


  for i in range(numEpoch):
    optim.zero_grad()
    Y_hat = model(train_X)   
    loss = lossFunc(Y_hat, train_Y)
    loss.backward()
    optim.step()

    if i % (numEpoch // 10) == 0:
        print(f'{i},\t{loss.item():.2f}')
        gt = evalGDness(Y_hat, train_Y)
        Y_hat_valid = model(valid_X)
        gv = evalGDness(Y_hat_valid, valid_Y)
        
        print('train:', gt)
        print('valid:', gv)
        
        gtp.append(gt[0])
        gtr.append(gt[1])
        gvp.append(gv[0])
        gvr.append(gv[1])

  import matplotlib.pyplot as plt
  plt.plot(gtp, label='gtp')
  plt.plot(gtr, label='gtr')
  plt.plot(gvp, label='gvp')
  plt.plot(gvr, label='gvr')
  plt.legend()
  plt.show()

  models.append(model)

#params = model.parameters()

#plt.plot((params[0][0].detach().numpy()))
#plt.show()
#for e in params:
    #print(e.shape)
    #print(e)

# predict testing data

test_X_pd = pd.read_csv(test_X_csv_name)

patientID = test_X_pd['PATIENT ID']

test_X_pd.drop(['PATIENT ID', 'admission_datetime'], axis=1, inplace=True)
test_X_pd['age'] = test_X_pd['age'].astype('float64')
test_X_pd.loc[ test_X_pd['sex'] == 'MALE', ['sex']] = 0
test_X_pd.loc[ test_X_pd['sex'] == 'FEMALE', ['sex']] = 1
test_X_pd['sex'] = test_X_pd['sex'].astype('float64')

for i in range(len(test_X_pd.loc[0,'vitals_temp_ed_first':'lab_hemoglobin'])):
    median = test_X_pd.iloc[:,i+3].median()
    test_X_pd.iloc[test_X_pd.iloc[:,i+3].isnull(), i+3] = median

dia_rowNum = len(test_X_pd)
dia_colNum = test_X_pd['ed_diagnosis'].nunique()
dia_onehot_np = np.zeros([dia_rowNum, dia_colNum])

for i in range(dia_rowNum):
    if (test_X_pd.loc[i, 'ed_diagnosis'] == 'sx_breathing_difficulty'):
        dia_onehot_np[i, 0] = 1
    elif (test_X_pd.loc[i, 'ed_diagnosis'] == 'sx_fever'):
        dia_onehot_np[i, 1] = 1
    elif (test_X_pd.loc[i, 'ed_diagnosis'] == 'sx_flu'):
        dia_onehot_np[i, 2] = 1
    elif (test_X_pd.loc[i, 'ed_diagnosis'] == 'sx_cough'):
        dia_onehot_np[i, 3] = 1
    else: # sx_others
        dia_onehot_np[i, 4] = 1
        
dia_onehot_pd = pd.DataFrame(dia_onehot_np)
dia_onehot_pd = dia_onehot_pd.rename(columns={0:'sx_breathing_difficulty', 1:'sx_fever', 2:'sx_flu', 3:'sx_cough', 4:'sx_others'})

test_X = pd.concat([test_X_pd, dia_onehot_pd], axis=1)
test_X.drop(['ed_diagnosis'], axis=1, inplace=True)

for i in range(len(test_X.iloc[0,:])):
    if (test_X.iloc[:, i].max() > 1):
        mean = test_X.iloc[:, i].mean()
        std = test_X.iloc[:, i].std()
        test_X.iloc[:, i] = (test_X.iloc[:, i] - mean) / std

test_X = test_X.to_numpy()

test_X = torch.tensor(test_X).float()


hospitalOutcome = np.zeros(len(patientID))
for model in models:
  test_Y = model(test_X)
  for i in range(len(hospitalOutcome)):
    if (test_Y[i, 0] >= 0.5):
        hospitalOutcome[i] += 0
    else: 
        hospitalOutcome[i] += 1

for i in range(len(hospitalOutcome)):
  if (hospitalOutcome[i] > 3):
      hospitalOutcome[i] = 1
  else :
      hospitalOutcome[i] = 0


'''
hospitalOutcome = np.zeros(len(patientID))
for i in range(len(hospitalOutcome)):
    if (test_Y[i, 0] >= 0.5):
        hospitalOutcome[i] = 0
    else: 
        hospitalOutcome[i] = 1
'''

# save files

for i in range(len(patientID)):
    print(f'{patientID[i]:5d}', int(hospitalOutcome[i]) )


with open("106011225.csv", mode='w', newline='') as submit_file:
    csv_writer = csv.writer(submit_file)
    csv_writer.writerow(['PATIENT ID', 'hospital_outcome'])
    for i in range(len(patientID)):
        row = [patientID[i]] + [int(hospitalOutcome[i])]
        csv_writer.writerow(row)

# official tutor

# NN

'''
sizeInput = len(train_X[0])
sizeHidden1 = 200
sizeHidden2 = 100
sizeOutput = 2

class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(sizeInput, sizeHidden1)
        self.fc2 = nn.Linear(sizeHidden1, sizeHidden2)
        self.fc3 = nn.Linear(sizeHidden2, sizeOutput)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = torch.sigmoid(self.fc3(x))
        return x

net = Net()

#params = list(net.parameters())
#print(len(params))
#print(params[1].size())

optimizer = optim.SGD(net.parameters(), lr=0.01)
criterion = nn.BCELoss()

for i in range(1000):
    optimizer.zero_grad()
    y_hat = net(train_X)
    loss = criterion(y_hat, train_Y)
    loss.backward()
    optimizer.step()
'''

#precision & recall

# NN 
'''
sizeInput = len(train_X[0])
sizeHidden1 = 20
sizeHidden2 = 20
sizeOutput = 1

stepSize = 0.001
momentum = 0.9
numEpoch = 10

model = nn.Sequential(
                    nn.Linear(sizeInput, sizeHidden1),
                    #nn.Linear(sizeInput, 2),
                    nn.ReLU(),
                    nn.Linear(sizeHidden1, sizeHidden2),
                    nn.ReLU(),
                    nn.Linear(sizeHidden2, sizeOutput),
                    nn.Sigmoid()
                    )

lossFunc = nn.CrossEntropyLoss()

def getLoss(prediction, groundTruth):
    TN = FN = FP = TP = 0
    #print(prediction.shape, groundTruth.shape)
    for i in range(len(prediction)):
        if (prediction[i] == 0 and groundTruth[i] == 0):
            TN += 1
        elif (prediction[i] == 0 and groundTruth[i] == 1):
            FN += 1
        elif (prediction[i] == 1 and groundTruth[i] == 0):
            FP += 1
        else:
            TP += 1
    
    precision = TP / (FP + TP)
    recall = TP / (FN + TP)

    return -1 * (precision + recall)


optim1 = torch.optim.Adam(model.parameters(), lr=stepSize)

#for i in range(numEpoch):
y_hat = model(train_X)
y_hat = torch.floor(y_hat + 0.5)
y_hat = torch.squeeze(y_hat, 1)
print(y_hat) 

  
    
    #optim.zero_grad()
    #loss.backward()
    #optim.step()

    #print(loss.grad)

#if numEpoch % 100 == 0:
#print('{},\t{:.2f}'.format(i, loss))



'''
