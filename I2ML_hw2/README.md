# Machine learning assignments: COVID-19 30-day mortality prediction


## Goal 

- Build a machine learning model to predict the 30-day mortality of patients
    - That is, whether a patient will die in the hospital within 30 days
- This is a **fully connected neural network** model


## Dataset

- There are 1834 patients in the training dataset and each with a unique ID and 47 attibutes
    - These attibutes are *age*, *sex*, *primary symptom*, *red blood cell count*, etc.
- 1834 hospital outcome for all patients will be provided and used as the labels

Part of dataset:
<img src="./assets/dataset_screenshot.png" width="800">


## Usage

1. Run [106011225_HW2_Model.py](./106011225_HW2_Model.py)
    ```
    $ python 106011225_HW2_Model.py
    ```
    The output file is [106011225.csv](./106011225.csv)

**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**
