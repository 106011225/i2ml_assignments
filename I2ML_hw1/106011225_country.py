import pandas as pd
import numpy as np
import csv
import math

origin_data = pd.read_csv('download.csv')
#origin_data = pd.read_csv('https://opendata.ecdc.europa.eu/covid19/casedistribution/csv')
#origin_data


# drop some not useful columns
origin_data.drop(["day", "month", "year"], axis=1, inplace=True) 
origin_data.drop(["geoId", "countryterritoryCode", "popData2019", "continentExp"], axis=1, inplace=True) 
origin_data.drop(["deaths", "Cumulative_number_for_14_days_of_COVID-19_cases_per_100000"], axis=1, inplace=True)


# fill up some blanks: fill according to upper data
##origin_data["Cumulative_number_for_14_days_of_COVID-19_cases_per_100000"].fillna(method='ffill', inplace=True)


# handle cases of negative num 
num_origin_data = origin_data._get_numeric_data()
num_origin_data[num_origin_data < 0] = 0


# fill up some data of missing days(not implement)


# group data by country
countryData = origin_data.groupby("countriesAndTerritories")


numRefdays = 7 # how many days are used to predict the next day?
predictionCsvHeader = []
predictionValue = np.zeros([numRefdays + 7, 210]) 
#mapeavg = 0

index = -1
for item in countryData: #item[0] is a country's name, item[1] is data

    predictionCsvHeader.append(item[0])
    index += 1

    print(index, item[0])

    item[1].drop(["dateRep", "countriesAndTerritories"], axis=1, inplace=True)
    cntryData = np.array(item[1])

    #only use recent ?? days for training
    cntryData = cntryData[:60, :]

    # extract features
    numData = len(cntryData) - numRefdays
    x = np.empty( [ numData , numRefdays ], dtype = float)
    y = np.empty( [ numData , 1 ], dtype = float)
    for day in range(numData):
        y[ day, 0 ] = cntryData[ day ]
        x[ day, : ] = cntryData[ day + 1 : day + numRefdays + 1, :].reshape(1, -1)
        
  
    # prepare $numRefdays data for predicting 10/9 ~ 
    predictionValue[7:, index] = cntryData[:numRefdays, :].reshape(1,-1)


    # split out validation set
    numData_tr = math.floor(len(x) * 0.8)
    numData_va = numData - numData_tr
    x_tr = x[: math.floor(len(x) * 0.8), :]
    y_tr = y[: math.floor(len(y) * 0.8), :]
    x_va = x[math.floor(len(x) * 0.8): , :]
    y_va = y[math.floor(len(y) * 0.8): , :]
    

    # training
    dim = numRefdays + 1
    w = np.zeros([dim, 1])
    wdummy = np.zeros([dim, 1])
    x_tr = np.concatenate((np.ones([numData_tr, 1]), x_tr), axis = 1).astype(float)
    learning_rate = 50
    iter_time = 1000
    alpha = 20 # regularization coefficient
    adagrad = np.zeros([dim, 1])
    eps = 0.0000000001
    for t in range(iter_time):
        
        '''
        loss = 0
        for i in range(numData_tr):
            if y_tr[i] != 0:
                loss += np.abs(1 - np.dot(x_tr[i], w) / y_tr[i])
        loss /= numData_tr
        if(t%100==0):
            print(str(t) + ":" + str(loss))
        '''

        gradient = np.zeros([dim, 1])
        for i in range(numData_tr):
            if y_tr[i,0] != 0:
                gradient += -1 / y_tr[i,0] * np.sign(1 - np.dot(x_tr[i], w) / y_tr[i,0]) * x_tr[i].reshape(-1, 1)
        gradient *= (1/numData_tr)
        wdummy[0] = w[0]
        gradient = gradient + alpha*(w - wdummy) # wdummy: exclude constant term from regularization term

        adagrad += gradient ** 2
        w = w - learning_rate * gradient / np.sqrt(adagrad + eps)


    '''
    #validation
    x_va = np.concatenate((np.ones([numData_va, 1]), x_va), axis = 1).astype(float)
    pre_y = np.dot(x_va, w)
    #for i in range(numData_va):
        #print(pre_y[i], y_va[i])

    mape = 0
    for i in range(numData_va):
        #print(pre_y[i], y_va[i])
        if y_va[i] != 0:
            mape += np.abs( (y_va[i] - pre_y[i]) / y_va[i])
    mape /= numData_va
    print("country", index, "mape", mape)
    mapeavg += mape
    '''

    # prediction
    # handle floor, negative
    for i in range(7):
        col = predictionValue[ 7-i:numRefdays+7-i , index].reshape(1, -1)
        col = np.concatenate((np.ones([1, 1]), col), axis = 1).astype(float)
        pre_col = np.dot(col, w)
        if pre_col < 0: 
            pre_col = 0
        predictionValue[6-i, index] = math.floor(pre_col)

#print("mape avg", mapeavg / 210)

# save prediction
with open("106011225_HW1.csv", mode='w', newline='') as submit_file:
    csv_writer = csv.writer(submit_file)
    csv_writer.writerow( [''] + predictionCsvHeader )
    for i in range(7):
        row = ['10/' + str(i + 9)] + list(predictionValue[6 - i])
        csv_writer.writerow(row)
