# Machine learning assignments: COVID-19 forecast (new cases per day)


## Goal

- Build a machine learning model to predict the future of COVID-19 new cases of each country
    - More specifically, predict cases of the next seven days in the future
- This is a **linear regression** model

## Dataset

The [dataset](./download.csv) is downloaded from ECDC website:  
https://www.ecdc.europa.eu/en/publications-data/download-todays-data-geographic-distribution-covid-19-cases-worldwide


## Usage

1. Run [106011225_country.py](./106011225_country.py)
    ```
    $ python 106011225_country.py
    ```
    The output file is [106011225_HW1.csv](./106011225_HW1.csv)


**Please refer to [SPEC.pdf](./SPEC.pdf) and [report.pdf](./report.pdf) for more details.**


